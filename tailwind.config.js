/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/index.html',
  '*.{html,js}'],
  
  

  theme: {
    borderWidth: {
      DEFAULT: '1px',
      '0': '0',
      '2': '2px',
      '3': '3px',
      '4': '4px',
      '6': '6px',
      '8': '8px',
    },
  
    extend: {
      colors:{
        "bookmark-purple":"#5267DF",
        "bookmark-red":"#FA5959",
        "bookmark-blue":"#242A45",
        "bookmark-grey":"#9194A2",
        "bookmark-white":"#f7f7f7",
      },
    },
    
    fontFamily: {
      Poppins:["Poppins, sans-serif"],
      google_sans: ["Google Sans, Roboto, RobotoDraft, Helvetica, Arial, sans-serif"]
    },

    container:{
      center: true,
      padding: "1rem",
      screens: {
        lg: "1124px",
        xl: "1124px",
        "2xl": "1124px",
      },
    },
    
  },
  variants:{
    extend:{},
  },
  plugins: [],
};
